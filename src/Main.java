import ffmpegfold.FrameApp;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author Faseldi
 */
public class Main {
    public static void main(String[] args){
        FrameApp f = new FrameApp();
        f.setVisible(true);
    }
}
