/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Options;

/**
 *
 * @author Faseldi
 */
public class Informations {
    public final String typePiste,codec,infos, piste, langue;
    public boolean pisteParDefaut;
    /*
    * creates a "Tuple" that contains a lot of informations about the medias
    */
    public Informations(String piste, String langue, String typePiste, String codec, String infos, boolean pisteParDefaut){
        this.typePiste = typePiste;
        this.codec=codec;
        this.infos=infos;
        this.piste=piste;
        if(langue.isEmpty()){
            this.langue="NA";
        }else{
            this.langue=langue;
        }
        this.pisteParDefaut=pisteParDefaut;
    }
    @Override
    public String toString(){
        return typePiste;
    }
}
