/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Options;

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.TreeCellRenderer;

/**
 *
 * @author Faseldi
 */
public class CustomThreeCellRenderer implements TreeCellRenderer{
    private ArrayList<Informations> infos;
    public CustomThreeCellRenderer(ArrayList<Informations> infos){
        this.infos = infos;
    }
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        JLabel label = new JLabel();
        try{
            int i = Integer.parseInt(value.toString());
            label.setText(infos.get(i).piste);
            value = infos.get(i).typePiste;
        }catch(NumberFormatException e){
        }
        switch(value.toString()){
            case "Video":
                label.setIcon(new ImageIcon("C:\\Users\\Faseldi\\Documents\\NetBeansProjects\\ffmpegFold\\src\\icons\\video.jpg"));
                break;
            case "Audio":
                label.setIcon(new ImageIcon("C:\\Users\\Faseldi\\Documents\\NetBeansProjects\\ffmpegFold\\src\\icons\\audio.png"));
                break;
            case "Sous-titres":
                label.setIcon(new ImageIcon("C:\\Users\\Faseldi\\Documents\\NetBeansProjects\\ffmpegFold\\src\\icons\\subs.png"));
                break;
            default:
                label.setText(value.toString());
        }
        if(label.getIcon() == null){
            label.setIcon(new ImageIcon("C:\\Users\\Faseldi\\Documents\\NetBeansProjects\\ffmpegFold\\src\\icons\\keyboard.png"));
        }
        label.setForeground(new Color(243,243,243));
        return label;
    }
    
}
