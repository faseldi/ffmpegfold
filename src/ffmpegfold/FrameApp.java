package ffmpegfold;

import OtherFrames.Liste;
import OtherFrames.MediaInfo;
import Options.Video;
import Options.Audio;
import Options.Plus;
import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.io.BufferedReader;
import java.io.IOException;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.UnsupportedLookAndFeelException;


/**
 *
 * @author Faseldi
 */
public class FrameApp extends javax.swing.JFrame {
    public String ffmpegBinDir;
    public boolean ffmpegDirOk;
    public boolean fileDirOk;
    public String pathToFileToConvert;
    public boolean destDirOk;
    public String destDir;
    public javax.swing.DefaultListModel<String> liste;
    private Video video;
    private Audio audio;
    private Plus plus;
    public java.util.ArrayList<String> tabSub;
    public java.util.ArrayList<String> tabSubLang;
    private long time;
    private SaveXML save;
    public java.util.ArrayList<Tuple> listeCommandes;
    private ResourceBundle bundle;
    /**
     * Creates new main form frame
     */
    public FrameApp() {
        bundle = ResourceBundle.getBundle("languages.lang",Locale.getDefault());
        initComponents();
        drop();
        initToolTip();
        destDir = javax.swing.filechooser.FileSystemView.getFileSystemView().getDefaultDirectory().getPath();
        ffmpegDirOk=false;fileDirOk=false;destDirOk=true;
        listeCommandes = new ArrayList<>();
        jButton10.setEnabled(false);
        File file = new File("FFMPEG\\bin");
        testerFFmpeg(file);
        desactiverButtonFFmpeg();
        video = new Video(this);
        audio = new Audio(this);
        plus = new Plus(this);
        jpanelVideo();
        save = new SaveXML(video, audio);
        try{
            save.load("FFMPEG\\bin\\save.xml");
        }catch(Exception e){}
        jScrollPane4.getVerticalScrollBar().setUI(new CustomScrollbar());
    }
    private void initToolTip(){
        jButton6.setToolTipText(bundle.getString("configvideo"));
        jButton7.setToolTipText(bundle.getString("configaudio"));
        jButton9.setToolTipText(bundle.getString("configsupp"));
        jButton3.setToolTipText(bundle.getString("selectffmpegbindir"));
        jButton4.setToolTipText(bundle.getString("selectordrag"));
        jButton5.setToolTipText(bundle.getString("selectedest"));
    }
    public javax.swing.JList getjList1(){
        return this.jList1;
    }
    private void testerFFmpeg(File file){
        if(file.exists()){
            for(File f : file.listFiles()){
                if(f.getName().equals("ffmpeg.exe")){
                    ffmpegDirOk = true;
                    ffmpegBinDir=file.getAbsolutePath();
                }
            }
        }
    }
    private void drop(){
        DropTarget dt = new DropTarget(){
            @Override
            public synchronized void drop(DropTargetDropEvent evt){
                try {
                    evt.acceptDrop(DnDConstants.ACTION_COPY);
                    List<File> droppedFiles = (List<File>) evt.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
                    if(droppedFiles.size() == 1 && droppedFiles.get(0).isDirectory()){
                        source(droppedFiles.get(0).getAbsolutePath());
                    }
                    if(destDirOk){
                        java.util.ArrayList<File> files = new java.util.ArrayList<>();
                        boolean sourceOk = false;
                        for (File file : droppedFiles) {
                            if(file!=null && !file.isDirectory()){
                                files.add(file);
                                if(!sourceOk){
                                    pathToFileToConvert = file.getParent();
                                    sourceOk = true;
                                }
                            }
                        }
                        File[] fi = new File[files.size()];
                        for(int i = 0;i<files.size();i++){
                            fi[i]=files.get(i);
                        }
                        boucleSources(fi);
                    }
                    else{
                        javax.swing.JOptionPane.showMessageDialog(null,bundle.getString("merciselect"));
                    }
                } catch (UnsupportedFlavorException | IOException | HeadlessException ex) {
                    System.err.println(ex);
                }
            }
        };
    jButton4.setDropTarget(dt);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jButton9 = new javax.swing.JButton();
        jPanel14 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jLabel3 = new javax.swing.JLabel();
        jButton10 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jButton8 = new javax.swing.JButton();
        jButton14 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jButton11 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jButton13 = new javax.swing.JButton();

        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.setMaximumSize(new java.awt.Dimension(200, 200));
        jPanel2.setPreferredSize(new java.awt.Dimension(200, 200));

        jPanel3.setBackground(new java.awt.Color(102, 102, 102));
        jPanel3.setToolTipText("");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 41, Short.MAX_VALUE)
        );

        jButton1.setText("jButton1");
        jButton1.setMaximumSize(new java.awt.Dimension(70, 25));
        jButton1.setMinimumSize(new java.awt.Dimension(70, 25));
        jButton1.setPreferredSize(new java.awt.Dimension(70, 25));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("nom");
        jLabel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 27, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel4.setMaximumSize(new java.awt.Dimension(200, 200));
        jPanel4.setPreferredSize(new java.awt.Dimension(200, 200));

        jPanel5.setBackground(new java.awt.Color(102, 102, 102));
        jPanel5.setToolTipText("");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 41, Short.MAX_VALUE)
        );

        jButton2.setText("jButton1");
        jButton2.setMaximumSize(new java.awt.Dimension(70, 25));
        jButton2.setMinimumSize(new java.awt.Dimension(70, 25));
        jButton2.setPreferredSize(new java.awt.Dimension(70, 25));

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("nom");
        jLabel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 27, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAutoRequestFocus(false);
        setBackground(new java.awt.Color(231, 231, 231));
        setResizable(false);

        jLabel7.setText("Faseldi @Version 2.5");

        jPanel13.setBackground(new java.awt.Color(163, 38, 213));
        jPanel13.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 174, Short.MAX_VALUE)
        );

        jButton9.setBackground(new java.awt.Color(40, 40, 40));
        jButton9.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton9.setForeground(new java.awt.Color(255, 255, 255));
        jButton9.setText(bundle.getString("plus"));
        jButton9.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(35, 35, 35), new java.awt.Color(35, 35, 35), new java.awt.Color(35, 35, 35), new java.awt.Color(35, 35, 35)));
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jPanel14.setBackground(new java.awt.Color(231, 231, 231));

        jList1.setBackground(new java.awt.Color(163, 38, 213));
        jList1.setForeground(new java.awt.Color(255, 255, 255));
        jList1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jScrollPane4.setViewportView(jList1);

        jLabel3.setFont(new java.awt.Font("Georgia", 0, 12)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("      ");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 833, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4)
                .addGap(1, 1, 1)
                .addComponent(jLabel3))
        );

        jButton10.setBackground(new java.awt.Color(40, 40, 40));
        jButton10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton10.setForeground(new java.awt.Color(255, 255, 255));
        jButton10.setText(bundle.getString("convertir"));
        jButton10.setToolTipText("");
        jButton10.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        jLabel4.setBackground(new java.awt.Color(231, 231, 231));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        jButton8.setBackground(new java.awt.Color(40, 40, 40));
        jButton8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton8.setForeground(new java.awt.Color(255, 255, 255));
        jButton8.setText(bundle.getString("addfile"));
        jButton8.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jButton14.setBackground(new java.awt.Color(40, 40, 40));
        jButton14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton14.setForeground(new java.awt.Color(255, 255, 255));
        jButton14.setText(bundle.getString("afficherfile"));
        jButton14.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButton14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton14ActionPerformed(evt);
            }
        });

        jButton3.setBackground(new java.awt.Color(40, 40, 40));
        jButton3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton3.setForeground(new java.awt.Color(255, 255, 255));
        jButton3.setText(bundle.getString("ffmpeg"));
        jButton3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(35, 35, 35), new java.awt.Color(35, 35, 35), new java.awt.Color(35, 35, 35), new java.awt.Color(35, 35, 35)));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setBackground(new java.awt.Color(40, 40, 40));
        jButton4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton4.setForeground(new java.awt.Color(255, 255, 255));
        jButton4.setText(bundle.getString("source"));
        jButton4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(35, 35, 35), new java.awt.Color(35, 35, 35), new java.awt.Color(35, 35, 35), new java.awt.Color(35, 35, 35)));
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setBackground(new java.awt.Color(40, 40, 40));
        jButton5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton5.setForeground(new java.awt.Color(255, 255, 255));
        jButton5.setText(bundle.getString("destination"));
        jButton5.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(35, 35, 35), new java.awt.Color(35, 35, 35), new java.awt.Color(35, 35, 35), new java.awt.Color(35, 35, 35)));
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton7.setBackground(new java.awt.Color(40, 40, 40));
        jButton7.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton7.setForeground(new java.awt.Color(255, 255, 255));
        jButton7.setText(bundle.getString("audio"));
        jButton7.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(35, 35, 35), new java.awt.Color(35, 35, 35), new java.awt.Color(35, 35, 35), new java.awt.Color(35, 35, 35)));
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton6.setBackground(new java.awt.Color(40, 40, 40));
        jButton6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButton6.setForeground(new java.awt.Color(255, 255, 255));
        jButton6.setText(bundle.getString("video"));
        jButton6.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(35, 35, 35), new java.awt.Color(35, 35, 35), new java.awt.Color(35, 35, 35), new java.awt.Color(35, 35, 35)));
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jPanel6.setBackground(new java.awt.Color(231, 231, 231));

        jButton11.setBackground(new java.awt.Color(0, 166, 208));
        jButton11.setFont(new java.awt.Font("Arial Black", 1, 12)); // NOI18N
        jButton11.setText("+");
        jButton11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(231, 231, 231), 2));
        jButton11.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jButton12.setBackground(new java.awt.Color(0, 166, 208));
        jButton12.setFont(new java.awt.Font("Arial Black", 1, 12)); // NOI18N
        jButton12.setText("-");
        jButton12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(231, 231, 231), 2));
        jButton12.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });

        jButton13.setBackground(new java.awt.Color(0, 166, 208));
        jButton13.setFont(new java.awt.Font("Arial Black", 1, 12)); // NOI18N
        jButton13.setText("X");
        jButton13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(231, 231, 231), 2));
        jButton13.setMargin(new java.awt.Insets(2, 2, 2, 2));
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jButton13, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(20, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton13, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jButton9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel14, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(7, 7, 7)
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton14, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jButton10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jButton14, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private boolean cont(String[] tab){
        for(String s: tab){
            if(s.equals("ffmpeg.exe")){
                ffmpegDirOk=true;
                return true;
            }
        }
        return false;
    };    
    private void source(String s){
        if(s != null){
            File dir = new File(s);
            pathToFileToConvert = s;
            boucleSources(dir.listFiles());
            jButton4.setForeground(new Color(255,255,255));
        }
        else{
            jButton4.setForeground(Color.red);
            fileDirOk = false;
        }
    }
    private void boucleSources(File[] files){
        liste = new javax.swing.DefaultListModel<>();
        for(File f : files){
            String fileToConvert = f.getName();
            if(fileToConvert.contains("  ")){
                f.renameTo(MediaInfo.getNomSansEspaces(f));
                fileToConvert = f.getName();
            }
            if(fileToConvert.contains(".mp4") || fileToConvert.contains(".mkv") || fileToConvert.contains(".avi")){
                liste.addElement(fileToConvert);
                fileDirOk = true;
            }
        }
        jList1.setModel(liste);
        try{
            video.getLabelNom().setText(bundle.getString("outupname")+liste.getElementAt(0));
        }catch(Exception e){
            video.getLabelNom().setText("");
        }
        ActiverValider();
        try{
            if(liste.size() > 0){
                actualiserST();
            }
        }catch(Exception e){e.printStackTrace();}
    }
    public void actualiserST(){
        String file;
        plus.getMap().removeAllItems();
        try{file = liste.getElementAt(jList1.getSelectedIndex());}
        catch(Exception e){file = liste.getElementAt(0);};
        try{
            Process p = Runtime.getRuntime().exec("cmd /c ffmpeg -i \""+pathToFileToConvert+"\\"+file+"\"", null, new File(ffmpegBinDir));   
            System.out.println("cmd /c ffmpeg -i \""+pathToFileToConvert+"\\"+file+"\"");
            System.out.println(ffmpegBinDir);
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            String line;
            tabSub = new java.util.ArrayList<>();
            tabSubLang = new java.util.ArrayList<>();
            while ((line = br.readLine()) != null) {
                if(line.contains("Stream") && line.contains("Subtitle") || line.contains("subtitle")){
                    for(int i = 0; i<line.length();i++){
                        if(line.charAt(i) == '#'){
                            String piste="";
                            for(int j =0; j < 10; j++){
                                try{
                                    int k = Integer.parseInt(line.charAt(i+j+1)+"");
                                    piste+=line.charAt(i+j+1);
                                }
                                catch(Exception e){
                                    if(line.charAt(i+j+1) == ':'){
                                        piste+=':';
                                    }
                                    else{
                                        tabSub.add(piste);
                                        piste+=" ";
                                        for(int m = 0; m<6; m++){
                                            piste+=line.charAt(i+j+m);
                                        }
                                        tabSubLang.add(piste);
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }catch(Exception e){
            System.err.println("rien à scan");
        }
        for(String s : tabSubLang){
            plus.getMap().addItem(s);
        }
    }
    private void ActiverValider(){
        if(ffmpegDirOk && fileDirOk){
            if(destDirOk){
                jButton10.setEnabled(true);
            }
        }
        else{
            if(!destDirOk){
                jButton10.setEnabled(false);
            }
        }
    }
    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        jpanelVideo();
    }//GEN-LAST:event_jButton6ActionPerformed
    private void jpanelVideo(){
        jPanel13.removeAll();
        jPanel13.add(video);
        video.setSize(jPanel13.getSize());
        jPanel13.validate();
        jPanel13.repaint();
        jPanel13.revalidate();
    }
    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        jPanel13.removeAll();
        jPanel13.add(audio);
        audio.setSize(jPanel13.getSize());
        jPanel13.validate();
        jPanel13.repaint();
        jPanel13.revalidate();
        jList1.setBackground(jPanel13.getBackground());
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        save.save("FFMPEG\\bin\\save.xml");
        ajouterListeAttente(true);
        SortiesEncodages sorties = new SortiesEncodages();
        sorties.setVisible(true);
        jPanel13.removeAll();
        jPanel13.add(sorties);
        sorties.setSize(jPanel13.getSize());
        jPanel13.validate();
        jPanel13.repaint();
        jPanel13.revalidate();
        ActualiserText actu = new ActualiserText(sorties, this);
        actu.start();
    }//GEN-LAST:event_jButton10ActionPerformed
    /**
     * 
     * @param demarrer that is needed to indicate if the launcher button is pressed, to display errors or not 
     */
    private void ajouterListeAttente(boolean demarre){
        if(liste == null || liste.isEmpty()){
            javax.swing.JOptionPane.showMessageDialog(null, bundle.getString("nothingtoadd"));
        }
        if(ffmpegDirOk && fileDirOk && destDirOk){
            String commande = "";
            String commandeExterne="";
            String vcodec  = video.getCodec().getSelectedItem().toString().equals("h264")? "libx264" : "libx265";
            if(plus.getCommandeExterne().getText().length() > 0){
                commandeExterne=" "+plus.getCommandeExterne().getText()+" ";
            }

            for(int j = 0; j < liste.getSize(); j++){
                String sub = "";
                if(plus.getSousTitresOk().isSelected()){
                    int numNom = (int) (Math.random() *(100000)) + 1000 ;
                    sub=" -vf \"ass="+numNom+".ass\" ";
                    commande+=ffmpegBinDir+"\\"+"ffmpeg -i \""+pathToFileToConvert+"\\"+liste.getElementAt(j)+"\" -an -vn -map "+tabSub.get(plus.getMap().getSelectedIndex())+" -c:s:0 ass "+"\""+numNom+".ass\" && ";
                }
                String audioCommandes=" -acodec ";
                if(!audio.getCodec().toString().equals("copy")){
                    audioCommandes+=audio.getCodec().getSelectedItem()+" -ab "+
                    audio.getDebit().getSelectedItem()+"k -mode "+audio.getMode().getSelectedItem();
                }
                else{
                    audioCommandes+="copy";
                }
                String crf = video.getCrfOk().isSelected()?" -crf "+video.getCrf().getValue():"";
                String debutcommande =ffmpegBinDir+"\\"+"ffmpeg -i "+"\""+pathToFileToConvert+"\\"+liste.getElementAt(j);
                String fincommande = " -vcodec "+vcodec+
                            " -preset "+video.getPreset().getSelectedItem()+audioCommandes+crf+commandeExterne+sub+
                            " \""+destDir;
                if(video.getNewName().isSelected()){
                    String nb = ((int) video.getNumDepart().getValue()+j)+"";
                    if(Integer.valueOf(nb) < 10){
                        nb = "0"+nb;
                    }
                    fincommande+="\\"+video.getBegNewName().getText()+" "+nb+" "+video.getEndNewName().getText()+"."+video.getExt().getSelectedItem()+"\"";
                }else{
                    fincommande+="\\"+liste.getElementAt(j)+"\"";
                }
                commande+=debutcommande+"\""+fincommande;
                boolean contains = false;
                for(Tuple t : listeCommandes){
                    if(t.getCommande().equals(commande)){
                        contains = true;
                        break;
                    }
                }
                if(contains && !demarre){
                    javax.swing.JOptionPane.showMessageDialog(null, bundle.getString("filesalready"));
                    break;
                }
                if(contains){
                    continue; // on va pas faire chier l'user avec un message en boucle quand il lance! :D
                }
                listeCommandes.add(new Tuple(commande, liste.getElementAt(j),pathToFileToConvert, destDir));
                commande="";
            }
        }
        else{
            javax.swing.JOptionPane.showMessageDialog(null, bundle.getString("destorsource"));
        }
    }
    class ActualiserText extends Thread{
        private final SortiesEncodages sorties;
        private final FrameApp f;
        public ActualiserText(SortiesEncodages sorties, FrameApp f){
            this.sorties=sorties;
            this.f=f;
        }
        private int getDureeEnSec(String line, boolean duration){
            if(duration){
                int indice = 0;
                int[] time = new int[3];
                String s = "";
                for(int i = 12; i<20; i++){
                    s+=line.charAt(i);
                }
                for(int  i = 0; i < s.length(); i+=3){
                    time[indice] = Integer.parseInt(s.charAt(i)+""+s.charAt(i+1));
                    indice++;
                }
                return time[0]*3600+time[1]*60+time[2];
            }
            String[] hMin = line.split("time=")[1].split(":");
            String sec = hMin[2].split(java.util.regex.Pattern.quote("."))[0];
            return (Integer.parseInt(hMin[0])*3600)+(Integer.parseInt(hMin[1])*60)+(Integer.parseInt(sec));
        }
        @Override
        public void run(){
            jButton10.setEnabled(false);
            jButton6.setEnabled(false);
            jButton7.setEnabled(false);
            jButton9.setEnabled(false);
            jButton4.setEnabled(false);
            jButton5.setEnabled(false);
            int dureeEnSec = 0;
            int numero = 0;int length = f.listeCommandes.size();
            double totalFrames = 0;
            String line;
            javax.swing.DefaultListModel<String> liste = f.liste;
            new Thread(new Runnable() {
                @Override
                public void run() {
                time=0;
                    while(true){
                        try{
                            Thread.sleep(1000);
                        }catch(Exception e){
                            System.out.println(e);
                        }
                        time++;
                    }
                }
            }).start();
            for(Tuple comm : f.listeCommandes){
                if(comm.getStatus() == Tuple.STATUS_DONE){
                    continue;
                }
                numero++;
                Process p;
                try{
                    p = Runtime.getRuntime().exec("cmd /c "+comm.getCommande(), null, new File(ffmpegBinDir));
                }catch(Exception e){
                    System.err.println("Erreur au lancement de la commande");
                    comm.setStatus(Tuple.STATUS_ERROR);
                    continue;
                }
                comm.setStatus(Tuple.STATUS_DONE);
                BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                StringBuilder sb = new StringBuilder();
                try{
                        while ((line = br.readLine()) != null) {
                            sb.append(line).append("\n");
                            sorties.actualiserText(line);
                            if(line.contains("Input #0")){
                                for(int i = 0; i<liste.size(); i++){
                                    jLabel4.setText("En cours : "+comm.getNom());
                                }
                            }
                            if(line.contains("Duration")){
                                dureeEnSec = getDureeEnSec(line, true);
                                totalFrames = dureeEnSec * 23.98;
                            }
                            if(line.contains("time=")){
                                try{
                                    double d = ((double) getDureeEnSec(line, false)/(double)dureeEnSec);
                                    jButton10.setText(Math.round(d*100)+"% "+numero+"/"+length);
                                    long l = Math.round(((d*totalFrames)/23.98));
                                    jLabel8.setText(bundle.getString("videoto")+getTemps(l));
                                }catch(Exception e){
                                    System.out.println(e);
                                }
                            }
                    }
                }catch(Exception e){
                    System.out.println(e);
                }
            }
            listeCommandes.clear();
            if(plus.couperPcApres()){
                try{
                    Process p = Runtime.getRuntime().exec("cmd /c shutdown.exe -s -t 0", null, new File(ffmpegBinDir));
                }catch(IOException e){
                    System.err.println("Le pc n'a pas pu s'eteindre...\n"+e);
                }
            }
            jButton10.setEnabled(true);
            jButton10.setText(bundle.getString("lancerconv"));
            jLabel4.setText("");
            jButton6.setEnabled(true);
            jButton7.setEnabled(true);
            jButton9.setEnabled(true);
            jButton4.setEnabled(true);
            jButton5.setEnabled(true);
            jLabel8.setText(bundle.getString("finishin")+getTemps(time));
        }
    }
    private String getTemps(long d){
        return java.time.LocalTime.ofSecondOfDay(d).toString();
    }
    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        if(jList1.getSelectedIndices().length == 1){
            if(jList1.getSelectedIndex() > 0){
                String h  = liste.getElementAt(jList1.getSelectedIndex()-1);
                String b  = liste.getElementAt(jList1.getSelectedIndex());
                liste.setElementAt(h, jList1.getSelectedIndex());
                liste.setElementAt(b, jList1.getSelectedIndex()-1);
                jList1.setSelectedIndex(jList1.getSelectedIndex()-1);
            }
        }
        actualiserNombre();
    }//GEN-LAST:event_jButton11ActionPerformed
    private boolean contient(int i, int[] t){
        for(int j : t){
            if(i == j){
                return true;
            }
        }
        return false;
    }
    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton13ActionPerformed
        if(jList1.getSelectedIndices().length > 0){
            java.util.List<String> s = new java.util.ArrayList<>();
            for(int i = 0; i< liste.getSize(); i++){
                if(!contient(i, jList1.getSelectedIndices())){
                    s.add(liste.getElementAt(i));
                }
            }
            liste.removeAllElements();
            for(String st : s){
                liste.addElement(st);
            }
        }
        actualiserNombre();
        actualiserNombreItems();
       // activerScanMedia();
    }//GEN-LAST:event_jButton13ActionPerformed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        if(jList1.getSelectedIndices().length == 1){
            if(jList1.getSelectedIndex() != jList1.getComponentCount()-1){
                try{
                    String h  = liste.getElementAt(jList1.getSelectedIndex()+1);
                    String b  = liste.getElementAt(jList1.getSelectedIndex());
                    liste.setElementAt(h, jList1.getSelectedIndex());
                    liste.setElementAt(b, jList1.getSelectedIndex()+1);
                    jList1.setSelectedIndex(jList1.getSelectedIndex()+1);
                }catch(Exception e){
                            System.out.println(e);
                    }
            }
        }
        actualiserNombre();
    }//GEN-LAST:event_jButton12ActionPerformed
    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        jPanel13.removeAll();
        jPanel13.add(plus);
        plus.setSize(jPanel13.getSize());
        jPanel13.validate();
        jPanel13.repaint();
        jPanel13.revalidate();
        plus.creerArbre();
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        ajouterListeAttente(false);
    }//GEN-LAST:event_jButton8ActionPerformed
    private Liste frameList;
    private void jButton14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton14ActionPerformed
        if(liste != null && liste.size() > 0 && (frameList == null || !frameList.isVisible())){ 
            frameList = new Liste(this);
        }
    }//GEN-LAST:event_jButton14ActionPerformed
    private void desactiverButtonFFmpeg(){
        if(ffmpegDirOk){
            jButton3.setEnabled(false);
            jButton3.setToolTipText(bundle.getString("buttondesact"));
        }
    }
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        String s  = promptFolder(bundle.getString("selectffmpegbindir"),ffmpegBinDir);
        if(s != null){
            File bin = new File(s);
            if(cont(bin.list())){
                ffmpegBinDir=s;
            }
            else{
                ffmpegDirOk = false;
            }
        }
        else{
            ffmpegDirOk = false;
        }
        ActiverValider();
        desactiverButtonFFmpeg();
      //  activerScanMedia();
    }//GEN-LAST:event_jButton3ActionPerformed
    /*private void activerScanMedia(){
        if(!liste.isEmpty()){
            plus.getScan().setEnabled(true);
        }
    }*/
    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        String s = promptFolder(bundle.getString("selectsourcedir"),pathToFileToConvert);
        source(s);
        actualiserNombre();
        actualiserNombreItems();
        if(liste!= null && !liste.isEmpty()){
            plus.creerArbre();
        }
    }//GEN-LAST:event_jButton4ActionPerformed
    private void actualiserNombre(){
        if(liste != null && liste.size() > 0){
            video.setInfosDepart(liste.getElementAt(0));
        }
    }
    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        String s  = promptFolder(bundle.getString("selectedest"),destDir);
        if(s != null){
            destDir = s;
            destDirOk = true;
            jButton5.setForeground(new Color(255,255,255));
        }
        else{
            jButton5.setForeground(Color.red);
            destDirOk = false;
        }
        ActiverValider();
    }//GEN-LAST:event_jButton5ActionPerformed

    private String promptFolder(String titre, String directory){
        LookAndFeel look = UIManager.getLookAndFeel();
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch(ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            System.err.println(ex);
        }
        javax.swing.JFileChooser fc = directory == null ? new javax.swing.JFileChooser() : new javax.swing.JFileChooser(directory);
        fc.setDialogTitle(titre);
        fc.setFileSelectionMode(javax.swing.JFileChooser.DIRECTORIES_ONLY);
        try {
            UIManager.setLookAndFeel(look);
        }catch(Exception ex1) {
            System.err.println(ex1);
        }
        return fc.showOpenDialog(this) == javax.swing.JFileChooser.APPROVE_OPTION ? fc.getSelectedFile().getAbsolutePath() : null;
    }
    private void actualiserNombreItems(){
        if(liste != null && liste.isEmpty()){
            jLabel3.setText("");
        }else{
            if(liste != null){
                jLabel3.setText(liste.size()+bundle.getString("videoplur"));
            }
        }
    }
    public ResourceBundle getBundle(){
        return bundle;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JList jList1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane4;
    // End of variables declaration//GEN-END:variables
}

