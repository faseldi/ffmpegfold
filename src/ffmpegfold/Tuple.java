package ffmpegfold;

/**
 *
 * @author Faseldi
 */
public class Tuple {
    private final String commande, nom, source, destination;
    private int status;
    /**
    * NOT DONE, value : 0
    */
    public static int STATUS_NOT_DONE = 0;
    /**
     * DONE, value : 1
     */
    public static int STATUS_DONE = 1;
    /**
     * ERROR, value : 2
     */
    public static int STATUS_ERROR = 2;
    /*
    * @param commande the ffmpeg command
    * @param nom the name
    * @param source the source of the file
    * @param destination the output of the file
    */
    public Tuple(String commande, String nom, String source, String destination){
        this.commande=commande;
        this.nom=nom;
        this.source=source;
        this.destination=destination;
        status=0;
    }
    public String getCommande(){
        return commande;
    }
    public String getNom(){
        return nom;
    }
    public String getSource(){
       return source; 
    }
    public String getDest(){
        return destination;
    }
    @Override
    public String toString(){
        return commande +" "+ nom;
    }
    public void setStatus(int s){
        status=s;
    }
    public int getStatus(){
        return status;
    }
}
