package ffmpegfold;

/**
 *
 * @author Faseldi
 */
public class SortiesEncodages extends javax.swing.JPanel {
    /**
     * Creates new form SortiesEncodages
     */
    int ligne = 0;
    /*
    * creates a jpanel for the output 
    * @deprecated
    */
    public SortiesEncodages() {
        initComponents();
        jScrollPane1.getVerticalScrollBar().setUI(new CustomScrollbar());
    }
    /*
    * update the text a the jtextpane
    */
    public void actualiserText(String line){
        try{
            jTextPane1.getDocument().insertString(0, line+"\n", null);
            ligne++;
            if(ligne == 1000){
                jTextPane1.setText("");ligne=0;
            }
        }catch(Exception e){
            System.out.println(e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();

        jScrollPane1.setBackground(new java.awt.Color(240, 149, 230));

        jTextPane1.setEditable(false);
        jTextPane1.setBackground(new java.awt.Color(163, 38, 213));
        jTextPane1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTextPane1.setForeground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setViewportView(jTextPane1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 724, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextPane jTextPane1;
    // End of variables declaration//GEN-END:variables
}
