/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ffmpegfold;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.plaf.basic.BasicScrollBarUI;


/**
 *
 * @author Faseldi
 */
public class CustomScrollbar extends BasicScrollBarUI{
    @Override
    protected void paintTrack(Graphics g, JComponent c, Rectangle bounds){
//        c.setBorder(new SoftBevelBorder(SoftBevelBorder.LOWERED, Color.RED, Color.RED));
        c.setBorder(new EmptyBorder(3,3,3,3));
        //c.setBorder(new LineBorder(Color.RED, 3, true));
        super.paintTrack(g, c, bounds);
    }
    @Override
    protected void paintThumb(Graphics g, JComponent c, Rectangle bounds){
        //bounds.setSize((int) (bounds.width-(0.1*bounds.width)),(int) (bounds.height-(0.1*bounds.height)));
        //c.setBorder(new SoftBevelBorder(SoftBevelBorder.LOWERED));
        super.paintThumb(g, c, bounds);
    }
    @Override
    protected void configureScrollBarColors(){
        trackColor = Color.WHITE;
        thumbColor = new Color(163,38,213);
        scrollbar.setBackground(Color.WHITE);
    }
    private JButton getButton(){
        JButton b = new JButton();
        b.setVisible(false);
        return b;
    }
    @Override
    protected JButton createIncreaseButton(int orientation){
        return getButton();
    }
    @Override
    protected JButton createDecreaseButton(int orientation){
        return getButton();
    }
}
