package ffmpegfold;

import Options.Video;
import Options.Audio;
import java.io.IOException;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 *
 * @author Faseldi
 */
public class SaveXML {
    private Video v;
    private Audio a;
    public SaveXML(Video v, Audio a){
        this.v=v;this.a=a;
    }
    /**
     * 
     * @param fileName
     * @return boolean that indicates if the load is successfull
     */
    public boolean load(String fileName){
        try{
            org.jdom2.Document document = new SAXBuilder().build(new java.io.File(fileName));
            v.getCodec().setSelectedItem(document.getRootElement().getChild("Video").getChildText("codec"));
            v.getCrf().setValue(Integer.parseInt(document.getRootElement().getChild("Video").getChildText("crf")));
            v.getPreset().setSelectedItem(document.getRootElement().getChild("Video").getChildText("vitesse"));
            
            a.getMode().setSelectedItem(document.getRootElement().getChild("Audio").getChildText("canaux"));
            a.getDebit().setSelectedItem(document.getRootElement().getChild("Audio").getChildText("debit"));
            a.getCodec().setSelectedItem(document.getRootElement().getChild("Audio").getChildText("codec"));
        }catch(JDOMException | IOException | NumberFormatException e){
            System.err.println(e);
            return false;
        }
        return true;
    }
    /**
     * 
     * @param fileName
     * @return boolean that indicates if the file has properlly been saved 
     */
    public boolean save(String fileName){
        try{
            org.jdom2.Document document = save(v,a);
            new org.jdom2.output.XMLOutputter(org.jdom2.output.Format.getPrettyFormat()).output(document, new java.io.FileOutputStream(fileName));
        }catch(Exception e){
            System.out.println(e);
            return false;
        }
        return true;
    }
    private org.jdom2.Document save(Video v, Audio a){
        org.jdom2.Document document = new org.jdom2.Document(new org.jdom2.Element("ffmpegFoldSave"));
        
        org.jdom2.Element video = new org.jdom2.Element("Video");
        org.jdom2.Element audio = new org.jdom2.Element("Audio");
        video.addContent(new org.jdom2.Element("codec").setText(v.getCodec().getSelectedItem().toString()));
        video.addContent(new org.jdom2.Element("crf").setText(v.getCrf().getValue()+""));
        video.addContent(new org.jdom2.Element("vitesse").setText(v.getPreset().getSelectedItem().toString()));
        
        audio.addContent(new org.jdom2.Element("canaux").setText(a.getMode().getSelectedItem().toString()));
        audio.addContent(new org.jdom2.Element("debit").setText(a.getDebit().getSelectedItem().toString()));
        audio.addContent(new org.jdom2.Element("codec").setText(a.getCodec().getSelectedItem().toString()));
        
        document.getRootElement().addContent(video);
        document.getRootElement().addContent(audio);
        return document;
    }
    
}
